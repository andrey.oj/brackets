# Brackets

Implement random player-names list and unique random ID's (do some random list generator).
Implement following features:
 - automatic players placement to 1st round
 - manual drag & drop to 1st round only! (prop which allows "drop" on match you need to implement as component prop, e.g. "allowDrop")  


# Run the application  

## Run compiled version  

Execute ``dist/index.html`` in your favorite browser  

## Run in dev mode

Installing dependencies: ``yarn install``  
Run application: ``yarn serve``  