
const createLeaves= (tree, level = -1, result = [], depth =0)=>{
  if(tree){
    if(level < 0 ){
      if(tree.children.length){
        createLeaves(tree.children[0],level,result, depth+1);
        createLeaves(tree.children[1],level,result, depth+1);
      }else {
        result.push(tree);
      }
    }else {
      if(level === depth){
        result.push(tree);
      }else {
        createLeaves(tree.children[0],level,result, depth+1);
        createLeaves(tree.children[1],level,result, depth+1);
      }
    }
  }

  return result;
};

export const leaves= (tree, level = -1)=>{
  const result = [];
  createLeaves(tree,level,result,0);
  return result;
};

