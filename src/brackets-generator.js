import {uuid} from 'vue-uuid';

const NAME_FOR_LEVEL = [
    'Final',
    'Semi-final',
    'Quarter-final',
];
const getNameForLevel = (level, max)=>{
  return NAME_FOR_LEVEL[level] || 'Level ' + (Math.log2(max)-level+1);
};

const nextLog=(count)=>{
  while(Math.log2(count) %1 !== 0){
    count++;
  }
  return count;
};

const createTree=(tree, level,maxPlayers, calls=0)=>{
  const countChildren = 2**level;

  if(countChildren < maxPlayers) {
    for (let i = 0; i < 2; i++) {
      const item = {
        name: getNameForLevel(level + 1, maxPlayers),
        children: [],
        player: {id: countChildren === maxPlayers/2 ? uuid.v4(): null, data: ''},
      };

      createTree(item, level + 1, maxPlayers, calls+2);
      tree.children.push(item);
    }
  }
};

export const generateBrackets = (count) => {
  const logCount = nextLog(count);
  const tree = {
    name: 'Final',
    children:[],
    player: {id: null, data: ''},
  };
  createTree(tree,0,logCount, 0);
  return tree;
};